// Critical -> Dependencies
import { Route, Switch, Redirect } from "react-router-dom";
import { useContext } from "react";
import AuthContext from "./app/store/auth-context";
import { PersonajeProvider } from "./app/context/PersonajeContext";

// Critical  -> Pages
import LoginPage from "./app/pages/login/login_page";
import HomePage from "./app/pages/home/home_page";
import SeleccionadoPage from "./app/pages/seleccionado/seleccionado_pages";

// Normal   -> Components

import NavBar from "./app/components/NavBar";

function App() {
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;

  return (
    <>
      {isLoggedIn && <NavBar />}
      <PersonajeProvider>
        <Switch>
          {!isLoggedIn && (
            <Route exact path="/">
              <LoginPage />
            </Route>
          )}
          {isLoggedIn && (
            <Route path="/home">
              <HomePage />
            </Route>
          )}
          {isLoggedIn && (
            <Route path="/personaje/:id">
              <SeleccionadoPage />
            </Route>
          )}
          {!isLoggedIn && (
            <Route path="*">
              <Redirect exact to="/" />
            </Route>
          )}
          {isLoggedIn && (
            <Route path="*">
              <Redirect exact to="/home" />
            </Route>
          )}
        </Switch>
      </PersonajeProvider>
    </>
  );
}

export default App;
