import {
  Container,
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
  Chip,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@mui/styles";
import ItemCard from "../home/itemCard";

const customItemSelected = makeStyles((theme) => ({
  mainWrapperSelected: {
    backgroundColor: "white",
    maxHeight: window.innerHeight - 105,
  },
  Card: {
    maxWidth: "230px!important",
  },
  Chip: {
    background: "#C6E46D!important",
    padding: "30px 10px!important",
    borderRadius: "30px!important",
  },
}));

export default function ItemSeleccionado() {
  const stylesitemSelected = customItemSelected();
  const { id } = useParams();
  const [personaje, setPersonaje] = useState("");

  useEffect(() => {
    axios.get("https://rickandmortyapi.com/api/character/" + id).then((res) => {
      let character = res.data;
      character.episode.map((urlEpisode) => {
        return axios
          .get(`${urlEpisode}`)
          .then((episodeInfo) =>
            console.log((character.nombrepisodio = episodeInfo.name))
          );
      });
      setPersonaje(character);
    });
  }, [id]);
  return (
    <>
      <Container className={stylesitemSelected.mainWrapperSelected}>
        <Grid container p={4}>
          <Grid item md={2}>
            <Card className={stylesitemSelected.Card} elevation={0}>
              <CardMedia component="img" src={personaje.image} />
              <CardContent>
                <Typography variant="h6">Nombre:{personaje.name}</Typography>
                <Typography variant="body1">#{personaje.id}</Typography>
                <Typography variant="body1">
                  <strong>Genero:</strong>
                  {personaje.gender}
                </Typography>
                <Typography variant="body1">
                  <strong>Especie:</strong>
                  {personaje.specie}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid
            item
            md={10}
            sx={{
              display: "flex",
              flexDirection: "column",
              textAlign: "center",
            }}
          >
            <Typography variant={"h3"}>Episodios donde aparece</Typography>
            <Grid container sx={{ rowGap: "20px" }}>
              {personaje.episode &&
                personaje.episode.map((el, key) => (
                  <Grid item md={4} sx={{ paddingX: "10px" }}>
                    <Chip
                      label={el}
                      key={key}
                      clickable
                      component="a"
                      href={el}
                      className={stylesitemSelected.Chip}
                    />
                  </Grid>
                ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
