import {
  AppBar,
  Box,
  CardMedia,
  Drawer,
  IconButton,
  List,
  ListItem,
  Typography,
  Button,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import logoBrand from "../assets/logo_color.png";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";
import { useState } from "react";

const customNavbarStyles = makeStyles((theme) => ({
  mainWrapper: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent!important",
  },
  mainItemWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    maxWidth: "1245px",
    paddingTop: "0",
    paddingBottom: "0",
    padding: "1.6em",
    marginLeft: "auto",
    marginRight: "auto",
    width: "fill-available",
  },
  logoBrand: {
    maxWidth: "319px",
  },
  Drawer: {
    height: "100vh",
    display: "flex!important",
    flexDirection: "column",
    justifyContent: "center!important",
    alignItems: "center!important",
  },
  ButtonLogout: {
    backgroundColor: "transparent!important",
    color: "black!important",
  },
}));

export default function NavBar() {
  const [toggle, setToggle] = useState(false);

  const logout = function () {
    // localStorage.clear("user");
  };
  const navBarStyles = customNavbarStyles();
  return (
    <>
      <AppBar
        className={navBarStyles.mainWrapper}
        elevation={0}
        position="static"
      >
        <Box className={navBarStyles.mainItemWrapper}>
          <IconButton
            onClick={() => setToggle(true)}
            size="large"
            edge="start"
            aria-label="open drawer"
            sx={{ mr: 2, color: "black", width: "65px", height: "65px" }}
          >
            <MenuIcon sx={{ fontSize: "45px" }} />
          </IconButton>
          <CardMedia
            className={navBarStyles.logoBrand}
            component={"img"}
            src={logoBrand}
          />
          <Typography sx={{ display: { xs: "none", md: "block" } }}>
            <Link to="/" onClick={logout()}>
              <Typography
                sx={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                Cerrar sesión
              </Typography>
            </Link>
          </Typography>
        </Box>
      </AppBar>
      <Drawer
        open={toggle}
        variant="temporary"
        onClose={() => setToggle(false)}
      >
        <List className={navBarStyles.Drawer}>
          <ListItem>
            <a href="https://es.wikipedia.org/wiki/Rick_y_Morty">
              Conoce a Rick & Morty
            </a>
          </ListItem>
          <ListItem>
            <a href="https://www.formulatv.com/series/rick-y-morty/capitulos/">
              Ver episodios
            </a>
          </ListItem>
        </List>
        <Button className={navBarStyles.ButtonLogout} variant="text">
          Cerrar Sesión
        </Button>
      </Drawer>
    </>
  );
}
