import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";

const customItemCard = makeStyles((theme) => ({
  Card: {
    maxWidth: "500px!important",
  },
}));
export default function ItemCard({ items }) {
  const stylesitemCard = customItemCard();
  return (
    <>
      {items.map((el, key) => (
        <Grid xs={12} md={"auto"} item key={key}>
          <Card className={stylesitemCard.Card}>
            <CardMedia component="img" src={el.image} />
            <CardContent>
              <Typography variant="h6">{el.name}</Typography>
              <Typography variant="body1">#{el.id}</Typography>
            </CardContent>
            <CardActions>
              <Link to={"personaje/" + el.id}>
                <Button variant="text">Ver</Button>
              </Link>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </>
  );
}
