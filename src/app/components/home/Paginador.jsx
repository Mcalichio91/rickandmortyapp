import * as React from "react";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import { useContext } from "react";
import { PersonajeContext } from "../../context/PersonajeContext";

export default function Paginador({ info }) {
  const { getPagination } = useContext(PersonajeContext);
  const [page, setPage] = React.useState(1);

  const handleChange = (event, value) => {
    console.log("PAGINA SELECCIONADA", value);
    setPage(value);
    getPagination(value);
  };

  // console.log("SOY EL PAGINADOR:::", info)
  return (
    <Stack spacing={2}>
      <Pagination count={info} page={page} onChange={handleChange} />
    </Stack>
  );
}
