import { Button, Box, Grid, List, ListItem } from "@mui/material";
import { useContext } from "react";
import { PersonajeContext } from "../../context/PersonajeContext";
import ItemCard from "./itemCard";
import Paginador from "./Paginador";

export default function Catalogo() {
  const { enviarResultados, info } = useContext(PersonajeContext);
  return (
    <>
      <Grid
        container
        sx={{
          flexFlow: "wrap",
          display: "flex",
          justifyContent: "flex-start",
          columnGap: "30px",
          rowGap: "30px",
          maxWidth: "1020px",
          margin: "0 auto",
        }}
      >
        <Grid
          item
          md={12}
          sx={{
            justifyContent: "center",
            display: "flex",
          }}
        >
          <List sx={{ display: "flex", flexDirection: "row" }}>
            <ListItem>
              <Button variant={"text"}>Personajes</Button>
            </ListItem>
            <ListItem>
              <Button variant={"text"}>Ubicaciones</Button>
            </ListItem>
            <ListItem>
              <Button variant={"text"}>Episodios</Button>
            </ListItem>
          </List>
        </Grid>
        <ItemCard items={enviarResultados} />
      </Grid>
      <Box
        sx={{ display: "flex", justifyContent: "center", marginTop: "3rem" }}
      >
        <Paginador info={info} />
      </Box>
    </>
  );
}
