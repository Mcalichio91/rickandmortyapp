import { Box, Card, CardMedia, Typography } from "@mui/material";
import image from "./../../assets/banner.jpg";
import { makeStyles } from "@mui/styles";
import Buscador from "../buscador/Buscador";

const CustomBannerStyles = makeStyles((theme) => ({
  Card: {
    boxShadow: "none!important",
    backgroundColor: "transparent!important",
  },
  BoxWrapper: {
    position: "relative",
    maxWidth: "1245px",
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    justifyContent: "center",
    overflow: "hidden",
  },
  CardMedia: {
    maxWidth: "1245px",
    width: "100%",
    maxHeight: "450px",
    borderRadius: "15px",
  },
  BoxItems: {
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.5)",
    bottom: 0,
    margin: "auto",
    width: "80%",
    height: "fill-available",
    padding: "20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  Title: {
    marginTop: "48px!important",
    fontFamily: "Roboto",
    fontSize: "64px!important",
    fontWeight: "Medium!important",
    color: "#C6E46D",
  },
  SubTitle: {
    fontFamily: "Roboto",
    fontSize: "32px!important",
    fontWeight: "100!important",
    lineHeight: "1.2!important",
    color: "#fff",
    maxWidth: "800px",
  },
  TextField: {
    marginTop: "45px!important",
    paddingLeft: "20px!important",
    paddingRight: "20px!important",
    backgroundColor: "rgba(244, 244, 244, 0.66)",
    border: "3px solid #CFCFCF!important",
    borderRadius: "25px",
    maxWidth: "650px!important",
  },
}));

export default function Banner() {
  const bannerStyles = CustomBannerStyles();
  return (
    <Card className={bannerStyles.Card}>
      <Box className={bannerStyles.BoxWrapper}>
        <CardMedia
          component={"img"}
          src={image}
          className={bannerStyles.CardMedia}
        ></CardMedia>
        <Box className={bannerStyles.BoxItems}>
          <Typography
            variant="h3"
            className={bannerStyles.Title}
            sx={{ display: { md: "block", xs: "none" } }}
          >
            Buscador de personajes
          </Typography>
          <Typography
            className={bannerStyles.SubTitle}
            sx={{ display: { md: "block", xs: "none" } }}
          >
            Utiliza el siguiente buscador para encontrar tu personaje favorito y
            su información detallada.
          </Typography>
          <Buscador />
        </Box>
      </Box>
    </Card>
  );
}
