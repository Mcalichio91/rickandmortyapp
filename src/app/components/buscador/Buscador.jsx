import { Button, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useContext, useState } from "react";
import { PersonajeContext } from "../../context/PersonajeContext";

const CustomBuscadorStyles = makeStyles((theme) => ({
  TextField: {
    marginTop: "45px!important",
    paddingLeft: "20px!important",
    paddingRight: "20px!important",
    backgroundColor: "rgba(244, 244, 244, 0.66)",
    border: "3px solid #CFCFCF!important",
    borderRadius: "25px",
    maxWidth: "650px!important",
  },
  Button: {
    marginTop: "2em!important",
    width: "206px",
    minHeight: "65px!important",
    backgroundColor: "#C6E46D!important",
    color: "black!important",
    borderRadius: "30px!important",
  },
}));

export default function Buscador() {
  const { getCharacters } = useContext(PersonajeContext);
  const buscadorStyles = CustomBuscadorStyles();
  const [buscar, setBuscar] = useState("");

  return (
    <>
      <TextField
        fullWidth
        className={buscadorStyles.TextField}
        onChange={(e) => setBuscar(e.target.value)}
        variant="filled"
        label={"Escribe el nombre del personaje aquí..."}
      ></TextField>

      <Button
        variant="contained"
        className={buscadorStyles.Button}
        onClick={() => getCharacters(buscar)}
      >
        Continuar
      </Button>
    </>
  );
}
