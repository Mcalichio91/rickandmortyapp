import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useContext, useState } from "react";
import logoBrand from "../../assets/logopositivo.png";
import axios from "axios";
import AuthContext from "./../../store/auth-context";
import { useHistory } from "react-router";

const customLoginStyles = makeStyles((theme) => ({
  Card: {
    background: "transparent!important",
    boxShadow: "none!important",
    width: "100%",
    textAlign: "center",
  },
  imgSubtitle: {
    fontSize: "24px!important",
    fontWeight: "800!important",
    marginTop: "-0.33rem!important",
    marginBottom: "1rem!important",
  },
  CardContent: {
    width: "400px",
    maxWidth: "400px",
    background: "white",
    borderRadius: "15px",
  },
  Box: {
    width: "316px",
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    flexDirection: "column",
    rowGap: "25px",
    justifyContent: "center",
    minHeight: "400px",
    textAlign: "left",
  },
  BoxTitle: {
    fontSize: "20px!important",
    fontWeight: "600!important",
    paddingLeft: "1rem",
    paddingRight: "1rem",
  },
  BoxParagraph: {
    fontSize: "18px!important",
  },
  TextField: {
    textAlign: "center!important",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#212121",
        borderRadius: "15px",
      },
    },
  },
  Button: {
    backgroundColor: "#212121!important",
    borderRadius: "15px!important",
    maxWidth: "240px",
    width: "100%",
    minHeight: "55px",
    marginX: "auto",
  },
}));

export default function FormularioLogin() {
  const authCtx = useContext(AuthContext);
  const history = useHistory();

  const [values, setValues] = useState({
    user: "",
    password: "",
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
    console.log("VALOR VALUES:::::::", values);
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const userLogin = function () {
    let userCredentials = {
      name: values.user,
      password: values.password,
    };
    console.log("CREDENCIALES:", userCredentials);
    axios
      .post("http://127.0.0.1:8080/login", userCredentials)
      .then((res) => {
        authCtx.login(res.data);
        history.replace("/home");
      })
      .catch((error) => console.error(error.response.data));
  };

  const loginStyles = customLoginStyles();

  return (
    <Card className={loginStyles.Card}>
      <CardMedia
        component={"img"}
        src={logoBrand}
        sx={{ maxWidth: "250px", marginX: "auto" }}
      />
      <Typography className={loginStyles.imgSubtitle}>APP</Typography>
      <CardContent className={loginStyles.CardContent}>
        <Box className={loginStyles.Box}>
          <Typography variant="h6" className={loginStyles.BoxTitle}>
            Inicia sesión a continuación
          </Typography>
          <Typography variant="body1" className={loginStyles.BoxParagraph}>
            Para iniciar sesión utiliza <strong>Marcelo</strong> en el usuario y{" "}
            <strong>1234</strong> en el campo de la contraseña.
          </Typography>
          <TextField
            label="Usuario"
            className={loginStyles.TextField}
            value={values.user}
            onChange={handleChange("user")}
          ></TextField>
          <FormControl className={loginStyles.TextField}>
            <InputLabel htmlFor="outlined-adornment-password">
              Contraseña
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={values.showPassword ? "text" : "Contraseña"}
              value={values.password}
              onChange={handleChange("password")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Contraseña"
            />
          </FormControl>
        </Box>
        <Button
          variant="contained"
          className={loginStyles.Button}
          onClick={userLogin}
        >
          Continuar
        </Button>
      </CardContent>
    </Card>
  );
}
