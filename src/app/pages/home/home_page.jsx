import { Box, Container } from "@mui/material";
import HomeLayout from "../../layouts/home/home_layout";

export default function HomePage() {
  return (
    <Container maxWidth={"1245px"}>
      <HomeLayout />
    </Container>
  );
}
