import LoginLayout from "../../layouts/login/login_layout";

export default function loginPage() {
  return (
    <>
      <LoginLayout />
    </>
  );
}
