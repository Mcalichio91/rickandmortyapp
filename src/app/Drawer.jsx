import { SwipeableDrawer } from "@mui/material";
import { useState } from "react";

export default function Drawer({ toggle }) {
  const [state, setState] = useState(false);
  return (
    <>
      <SwipeableDrawer
        anchor={"left"}
        open={state}
        onClose={setState(false)}
        onOpen={state(true)}
      >
        Hola
      </SwipeableDrawer>
    </>
  );
}
