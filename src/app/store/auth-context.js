import React, { useState } from "react";

const AuthContext = React.createContext({
  user: "",
  isLoggedIn: false,
  login: (user) => {},
  logout: () => {},
});

export const AuthContextProvider = (props) => {
  const initialUser = localStorage.getItem("user");
  const [user, setUser] = useState(initialUser);

  const userIsLoggedIn = !!user;
  const loginHandler = (data) => {
    setUser(data);
    localStorage.setItem("user", data);
  };
  const logoutHandler = () => {
    setUser(null);
    localStorage.removeItem("user");
  };

  const contextValue = {
    user: user,
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
