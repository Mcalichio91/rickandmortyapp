import { createContext, useEffect, useState } from "react";
import axios from "axios";

export const PersonajeContext = createContext();

export const PersonajeProvider = ({ children }) => {
  const [characterResults, setCharacterResults] = useState([]);
  const [enviarResultados, setEnviarResultados] = useState([]);
  const [palabraConsultada, setPalabraConsultada] = useState();
  const [info, setInfo] = useState();

  useEffect(() => {
    axios.get("https://rickandmortyapi.com/api/character/").then((res) => {
      setEnviarResultados(res.data.results);
      setInfo(res.data.info.pages);
    });
  }, []);

  function getCharacters(element) {
    console.log("entre en caracters y el elemente es: " + element);
    setPalabraConsultada(element);
    setCharacterResults([]);
    if (element) {
      axios
        .get(`https://rickandmortyapi.com/api/character/?name=${element}`)
        .then((res) => {
          setEnviarResultados(res.data.results);
          setInfo(res.data.info.pages);
        })
        .catch((error) => error);
    }
  }

  function getPagination(pagina) {
    setCharacterResults([]);
    if (palabraConsultada) {
      axios
        .get(
          `https://rickandmortyapi.com/api/character/?page=${pagina}&name=${palabraConsultada}`
        )
        .then((res) => {
          setEnviarResultados(res.data.results);
          console.log(res.data.info);
        })
        .catch((error) => error);
    } else {
      axios
        .get(`https://rickandmortyapi.com/api/character/?page=${pagina}`)
        .then((res) => {
          setEnviarResultados(res.data.results);
          console.log(res.data.info);
        })
        .catch((error) => error);
    }
  }

  return (
    <PersonajeContext.Provider
      value={{
        getCharacters,
        getPagination,
        characterResults,
        enviarResultados,
        info,
      }}
    >
      {children}
    </PersonajeContext.Provider>
  );
};
