import { Box, Grid } from "@mui/material";
import Catalogo from "../../components/home/Catalogo";
import Banner from "./../../components/home/Banner";
import NavBar from "./../../components/NavBar";

export default function HomeLayout() {
  return (
    <>
      <Box>
        <Banner />
        <Catalogo />
      </Box>
    </>
  );
}
