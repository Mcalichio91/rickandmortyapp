import { Grid, Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
import FormularioLogin from "../../components/login/FormularioLogin";

const customLoginStyles = makeStyles((theme) => ({
  Container: {
    display: "flex!important",
    alignItems: "center",
    justifyContent: "center",
  },
}));
export default function LoginLayout() {
  const loginStyles = customLoginStyles();
  return (
    <>
      <Grid container sx={{ minHeight: "100vh", width: "100vw" }}>
        <Container className={loginStyles.Container}>
          <Grid item>
            <FormularioLogin></FormularioLogin>
          </Grid>
        </Container>
      </Grid>
    </>
  );
}
