module.exports = function(app){
	const Cors = require('../config/Cors.js');
	const loginServices 	= require ('../services/loginServices');
	// const authorizationService = require ('../services/authorizationService.js');
	app.use(Cors.cors(Cors.corsOptions));

	app.post('/login', async function(req, res){
		let userData = req.body;
        let response = await loginServices.getUser(userData)
		res.status(response.status)
        res.send(response.message);
	})
}