module.exports = function(app){
	const Cors = require('../config/Cors.js');
	const charactersServices 	= require ('../services/charactersServices');
	// const authorizationService = require ('../services/authorizationService.js');
	app.use(Cors.cors(Cors.corsOptions));

	app.get('/character', async function(req, res){
        let response = await charactersServices.getAuthor()
        res.send(response);
	})
}