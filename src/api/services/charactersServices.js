const axios = require('axios');
let authorRepositorie = require('./../repositories/authorRepositorie')
const urlBase = 'https://rickandmortyapi.com/api/';

async function getCharactersApi () {
    let response = axios.get(urlBase + 'character/')
    return response;
}

const CharactersServices = {
    getAuthor: async () => {
        let dbAuthor = await authorRepositorie.getAuthor()
        let getCharacters = await getCharactersApi()
        
        
        if(dbAuthor.success && getCharacters.data !== null){
            let authorResult = dbAuthor.result
            let character = {result: getCharacters.data.results, author: authorResult};
            // character.author = authorResult.author
            let response  = {response: character};
            return response;
        }else{
            let response  = {error: "Error al obtener los personajes"}
            return response;
        }
    }
}

module.exports = CharactersServices