let loginRepositorie = require('./../repositories/loginRepositorie')

const LoginServices = {
    getUser: async (req)  => {
        console.log("RECIBO DEL FRONT:", req)
        let response          = {status: 401, message: 'Usuario o contraseña invalidos'}
        let responseUserLogin = await loginRepositorie.getUser();
        console.log("RECIBO DEL BACKEND:", responseUserLogin)
        if(req.name === responseUserLogin.name && 
            req.password === responseUserLogin.password)
            response  = {status: 200, message: 'Ok'}
        return response
    }

}

module.exports = LoginServices