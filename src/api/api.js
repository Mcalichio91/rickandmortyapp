const express 		= require('express');
const app 			= express();
const bodyParser 	= require('body-parser');
require('dotenv').config()

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
//app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
//app.use(express.urlencoded({ extended: true }));

require('./controllers/charactersController.js')(app)
require('./controllers/loginController.js')(app)

global.Cors 		= require('./config/Cors.js');
const Cors 			= global.Cors
app.use(Cors.cors(Cors.corsOptions));

//LEVANTAR LA API
app.listen( 8080 ,function(){
	console.log(`Rick&Morty API lenvantada en puerto 8080`);
})	